# selenium sample
## About
dockerでselenium+chrome driverを使ってなんかするやつ

## Usage
```
docker-compose build
docker-compose up -d
docker-exec -it selenium_sample_python_app_1 bash
```
`python_app/src`が`/selenium`にマウントされる

インストールされるchromeのバージョンを指定してないからchrome driverのバージョンと合わなくてうまくいかないかも．いい感じのバージョンのchrome driverを入れるようにしましょう．
